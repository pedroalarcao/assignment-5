class Appointment {

    String description;
    int year;
    int month;
    int day;


    Appointment(String description, int year, int month, int day) {
        this.year = year;
        this.month = month;
        this.day = day;
        this.description = description;
    }

    public boolean occursOn(int year, int month, int day) {


        return true;
    }

    public int getDay() {
        return day;
    }

    public int getMonth() { return month; }

    public int getYear() {
        return year;
    }
}
