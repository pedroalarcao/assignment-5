import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Scanner;
import java.time.LocalDate;

public class P9_4 {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        // ArrayList of Appointment objects
        ArrayList<Appointment> appointments = new ArrayList<Appointment>();


        char selection;
        do {
            System.out.println("Select an option: A for add an appointment, C for checking, Q to quit: ");
            selection = input.next().charAt(0);
            if (selection == 'A') {
                //since it comes after a next(), we need to have a nextLine() so that it doesn't conflict with next input
                input.nextLine();

                System.out.println("Enter the type (O - OneTime, D - Daily, or M - Monthly): ");
                char typeInput = input.next().charAt(0);
                input.nextLine();

                System.out.println("Enter the date (yyyy-mm-dd): ");
                String dateInput = input.next();

                //sets date is at this format
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                //stores date at localDate as a LocalDate object which helps us get() each value of date separately
                LocalDate localDate = LocalDate.parse(dateInput, formatter);
                //since it comes after a next(), we need to have a nextLine() so that it doesn't conflict with next input
                input.nextLine();

                System.out.println("Enter the description: ");
                String descriptionInput = input.nextLine();

                if (typeInput == 'O') {
                    //creates a Onetime object with the param that user inputted and stores it in ArrayList
                    appointments.add(new Onetime(descriptionInput, localDate.getYear(), localDate.getMonthValue(), localDate.getDayOfMonth()));

                }
                if (typeInput == 'D') {
                    //creates a Daily object with the param that user inputted and stores it in ArrayList
                    appointments.add(new Daily(descriptionInput, localDate.getYear(), localDate.getMonthValue(), localDate.getDayOfMonth()));
                }
                if (typeInput == 'M') {
                    //creates a Monthly object with the param that user inputted and stores it in ArrayList
                    appointments.add(new Monthly(descriptionInput, localDate.getYear(), localDate.getMonthValue(), localDate.getDayOfMonth()));
                }
            }
            if (selection == 'C') {
                System.out.println("Enter year: ");
                int year = input.nextInt();
                //since it comes after a nextInt(), we need to have a nextLine() so that it doesn't conflict with next input
                input.nextLine();
                System.out.println("Enter month: ");
                int month = input.nextInt();
                //since it comes after a nextInt(), we need to have a nextLine() so that it doesn't conflict with next input
                input.nextLine();
                System.out.println("Enter day: ");
                int day = input.nextInt();

                System.out.println("Appointments: ");

                //Traverses the ArrayList of appointments
                for (Appointment appointment : appointments) {

                    //if Appointment object at this index returns true for method occursOn, it will print out description of object
                    if (appointment.occursOn(year, month, day)) {
                        System.out.println(appointment.description);
                    }
                }
            }
            System.out.println(" ");
        }while (selection != 'Q');

    }
}