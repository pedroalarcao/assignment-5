import java.util.Scanner;

public class P9_3 {
    public static void main(String[] args) {

        //array of Appointment objects
        Appointment[] appointments = new Appointment[3];
        appointments[0] = new Daily("Walk dog", 2020, 10, 7);
        appointments[1] = new Monthly("Pay rent", 2020, 11, 1);
        appointments[2] = new Onetime("Complete essay", 2020, 12, 3);

        System.out.println("Enter a date (year, month, day) to list "
                + "appointments: ");
        Scanner input = new Scanner(System.in);
        int year = input.nextInt();
        int month = input.nextInt();
        int day = input.nextInt();

        //traverse array of appointments
        for (int i = 0; i < appointments.length; i++)
        {
            //if Appointment object at this index returns true for method occursOn, it will print out description of object
            if (appointments[i].occursOn(year, month, day))
            {
                System.out.println(appointments[i].description);
            }
        }

    }
}
