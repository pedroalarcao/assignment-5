class Onetime extends Appointment {

    Onetime(String description, int year, int month, int day) {
        super(description, year, month, day);
    }

    // Has to be same year && same month && same day
    //returns true if those conditions are met
    public boolean occursOn(int year, int month, int day) {

        return (getYear() == year) && (getMonth() == month) && (getDay() == day);
    }

}
