import java.awt.Rectangle;
import java.util.InputMismatchException;
import java.util.Scanner;

class BetterRectangle extends Rectangle {

    public BetterRectangle(int x, int y, int width, int height) {

        // I wasn't sure why we needed setLocation for this assignment. If it's useful, I'm not sure why.
        super.setLocation(x, y);
        // Called method in Rectangle class setSize which set the size of rectangle
        super.setSize(width, height);

    }

    public double getPerimeter() {

            return (2 * width) + (2 * height);

        }

        double getArea () {

            return width * height;

        }


}
public class E9_13 {
    public static void main(String[] args) {

        int x = 0;
        int y = 0;
        int width = 0;
        int height = 0;

        Scanner input = new Scanner(System.in);

        try{
            System.out.println("Type in the width of your rectangle: ");
            width = input.nextInt();
            System.out.println("Type in the height of your rectangle: ");
            height = input.nextInt();
            if (width <= 0 || height <= 0){
                throw new IllegalArgumentException();
            }
        } catch (InputMismatchException exception) {
            System.out.println("Number had to be an integer. Program terminated.");

            // I wanted the program to terminate if an exception occurred.
            // I'm not sure if that's what was intended from this assignment or if this is following best practices.
            System.exit(1);
        } catch (IllegalArgumentException exception){
            System.out.println("Number had to be an amount bigger than 0. Program terminated.");

            // I wanted the program to terminate if an exception occurred.
            // I'm not sure if that's what was intended from this assignment or if this is following best practices.
            System.exit(1);
        }


        BetterRectangle rectangle = new BetterRectangle(x, y, width, height);

        System.out.println("The perimeter of your rectangle is: " + rectangle.getPerimeter());
        System.out.println("The area of your rectangle is: " + rectangle.getArea());

    }
}
