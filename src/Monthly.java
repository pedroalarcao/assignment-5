class Monthly extends Appointment {

    Monthly(String description, int year, int month, int day) {
        super(description, year, month, day);
    }

    // Has to be same or later year but, if same year,  has to be same or later month. Has to be same day.
    //returns true if those conditions are met
    public boolean occursOn(int year, int month, int day) {

        if (year == getYear() && month >= getMonth() && day == getDay())
            return true;
        if (year > getYear() && day == getDay())
            return true;
        else
            return false;
    }
}
