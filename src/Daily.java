class Daily extends Appointment {

    Daily(String description, int year, int month, int day) {
        super(description, year, month, day);
    }

    // Has to be same or later year (Later year auto includes)
    // If same year, has to be same or later month (Later month auto includes)
    // If same month, has to be same or later day
    //returns true if those conditions are met
    public boolean occursOn(int year, int month, int day) {

        if (year > getYear())
            return true;
        if (year == getYear() && month > getMonth())
            return true;
        if (year == getYear() && month == getMonth() && day > getDay())
            return true;
        if (year == getYear() && month == getMonth() && day == getDay())
            return true;

        return false;
    }
}
